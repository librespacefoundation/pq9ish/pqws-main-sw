/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FSM_H_
#define FSM_H_

#include "cmsis_os.h"

/**
 * Hold of time step in seconds, must be less than WD timeout
 */

typedef enum {
	FSM_LOW_POWER_MODE,     	//!< FSM_LOW_POWER_MODE
	FSM_TC_MODE,            	//!< FSM_TC_MODE
} fsm_state_t;

int
fsm_task();

#endif /* FSM_H_ */
