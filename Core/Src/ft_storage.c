/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Drivers/AX5043/include/fec.h>
#include <string.h>
#include "ft_storage.h"
#include "error.h"
#include "utils.h"
#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_flash.h"
#include <string.h>

#define FT_STORAGE_INIT			0x0886F03E

/**
 * Initialize a memory region for keeping fault tolerant state
 * @param h the fault tolerant structure handle
 * @param obj_size the size of the object that will be stored
 * @param addr starting address of a flash page
 * @return 0 on success or negative error code
 */
int
ft_storage_init(struct ft_storage *h, uint32_t obj_size, uint32_t addr)
{
	if (!h || !addr || !obj_size) {
		return -INVAL_PARAM;
	}
	/* Check if the available space is enough */
	if (obj_size > 223) {
		return -INVAL_PARAM;
	}

	h->ft0.addr = addr;
	h->ft0.obj_size = obj_size;
	h->ft0.init = FT_STORAGE_INIT;
	h->ft1 = h->ft0;
	h->ft2 = h->ft0;
	return NO_ERROR;
}

/**
 * Applies TMR on the fault tolerant structure members
 * @param h the fault tolerant structure handle
 */
static void
tmr_struct(struct ft_storage *h)
{
	h->ft0.addr = tmr(h->ft0.addr, h->ft1.addr, h->ft2.addr);
	h->ft0.obj_size = tmr(h->ft0.obj_size, h->ft1.obj_size, h->ft2.obj_size);
	h->ft0.init = tmr(h->ft0.init, h->ft1.init, h->ft2.init);
	h->ft1 = h->ft0;
	h->ft2 = h->ft0;
}

/**
 * Apply TMR on the RS encoded buffer. This function assumes that the
 * tmr_struct() has been called before to ensure error correction on the
 * addr field
 * @param h the fault tolerant structure handle
 */
static void
tmr_buffer(struct ft_storage *h)
{
	HAL_FLASH_Unlock();
	uint8_t *cp0 = (uint8_t *)h->ft0.addr;
	uint8_t *cp1 = (uint8_t *)h->ft0.addr + FLASH_PAGE_SIZE;
	uint8_t *cp2 = (uint8_t *)h->ft0.addr + 2 * FLASH_PAGE_SIZE;
	for (size_t i = 0; i < 255; i++) {
		h->buffer[i] = (cp0[i] & cp1[i]) | (cp0[i] & cp2[i])
		               | (cp1[i] & cp2[i]);
	}
	HAL_FLASH_Lock();
}

/**
 * Read from the fault tolerant memory region
 * @note in case a recoverable error is detected, this function writes back
 * the corrected data using the ft_storage_write() function
 * @param h the fault tolerant structure handle
 * @param out memory to copy the result
 * @return 0 on success or negative error code
 */
int
ft_storage_read(struct ft_storage *h, void *out)
{
	if (!h) {
		return -INVAL_PARAM;
	}
	tmr_struct(h);
	if (h->ft0.init != FT_STORAGE_INIT) {
		return -FT_STORAGE_INIT_ERROR;
	}
	tmr_buffer(h);
	int ret = rs_decode(h->buffer, h->ft0.obj_size + 32);
	/* Too many errors, cannot recover*/
	if (ret < 0) {
		return -FT_STORAGE_COR_MEM;
	}
	/*
	 * Some errors occurred, but ECC recovered the erroneous data!
	 * Perhaps it is a good opportunity to write back the corrected data
	 */
	else if (ret > 0) {
		memcpy(out, h->buffer, h->ft0.obj_size);
		return ft_storage_write(h, out);
	} else {
		memcpy(out, h->buffer, h->ft0.obj_size);
	}
	return NO_ERROR;
}

/**
 * Writes data to the fault tolerant memory region. First the RS parity checksum
 * is computed and then 3 copies of the data + parity are stored in different
 * regions of the flash memory
 * @note storage on the flash memory is highly dependent on the selected MCU.
 * This function should ONLY be used as it is in the STM32-L476 MCU
 * @param h the fault tolerant structure handle
 * @param b data to write
 * @return 0 on success or negative error code
 */
int
ft_storage_write(struct ft_storage *h, const void *b)
{
	if (!h) {
		return -INVAL_PARAM;
	}
	tmr_struct(h);
	if (h->ft0.init != FT_STORAGE_INIT) {
		return -FT_STORAGE_INIT_ERROR;
	}
	/* Pad and copy the data into the internal buffer */
	memset(h->buffer, 0, 256);
	memcpy(h->buffer, b, h->ft0.obj_size);
	/* Apply RS(223, 255) on the data */
	rs_encode(h->buffer + h->ft0.obj_size, h->buffer, h->ft0.obj_size);

	/* Write the data to the three pages */
	for (uint32_t i = 0; i < 3; i++) {
		uint32_t error;
		FLASH_EraseInitTypeDef erase;

		uint32_t addr = h->ft0.addr + i * FLASH_PAGE_SIZE;
		HAL_FLASH_Unlock();
		erase.TypeErase = FLASH_TYPEERASE_PAGES;
		if (((addr - 0x08000000) / FLASH_PAGE_SIZE) / 256) {
			erase.Banks = FLASH_BANK_2;
		} else {
			erase.Banks = FLASH_BANK_1;
		}
		erase.Page = (addr - 0x08000000) / FLASH_PAGE_SIZE;
		erase.NbPages = 1;

		int ret = HAL_FLASHEx_Erase(&erase, &error);
		if (ret) {
			HAL_FLASH_Lock();
			return -FLASH_ERASE_ERROR;
		}

		for (size_t i = 0; i < 256 / sizeof(uint64_t); i++) {
			uint64_t x;
			memcpy(&x, &h->buffer[i * sizeof(uint64_t)], sizeof(uint64_t));
			ret = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, addr, x);
			if (ret) {
				HAL_FLASH_Lock();
				return -FLASH_PROGRAM_ERROR;
			}
			addr += sizeof(uint64_t);
		}
		HAL_FLASH_Lock();
		/*
		 * Reduce the risk of more than two consecutive pages are
		 * destroyed by introducing a small amount of delay
		 */
		HAL_Delay(20);

	}
	return NO_ERROR;
}
